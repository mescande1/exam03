/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mini_paint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 14:43:42 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/07 17:49:28 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

typedef struct s_object {
	char	type;
	float	x;
	float	y;
	float	rad;
	char	fil;
	struct s_object		*next;
}	t_obj;

typedef struct s_bas {
	int		width;
	int		height;
	char	background;
	t_obj	*elem;
	char	tab[301][301];
}	t_bas;

int		ft_strlen(char *str)
{
	int i = 0;

	while (str && str[i])
		i++;
	return i;
}

void	ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

void	*ft_memmalloc(int size)
{
	char *ptr;

	ptr = malloc(size);
	while (--size >= 0)
		ptr[size] = 0;
	return (ptr);
}

int		parse(char *filename, t_bas *all)
{
	t_obj	*tmp;
	int		first_round = 1;
	int		ret_val;
	FILE	*file = fopen(filename, "r");
	t_obj	*last;

	ret_val = fscanf(file, " %d %d %c ", &(all->width), &(all->height), &(all->background));
	if (ret_val != 3 || all->width <= 0 || 300 < all->width || all->height <= 0 || 300 < all->height)
		return (1);
	tmp = ft_memmalloc(sizeof(t_obj));
	while((ret_val = fscanf(file, " %c %f %f %f %c ", &(tmp->type), &(tmp->x), &(tmp->y), &(tmp->rad), &(tmp->fil))) != -1)
	{
		if (ret_val != 5 || (tmp->type != 'c' && tmp->type != 'C') || tmp->rad <= 0)
		{
			free(tmp);
			while (all->elem)
			{
				tmp = all->elem;
				all->elem = tmp->next;
				free(tmp);
			}
			return (1);
		}
		if (first_round)
			all->elem = tmp;
		else
			last->next = tmp;
		last = tmp;
		tmp = ft_memmalloc(sizeof(t_obj));
		first_round = 0;
	}
	free(tmp);
	return (0);
}

float	ft_abs(float x)
{
	if (x < 0)
		return (-x);
	return x;
}

void	fillit(t_bas *all)
{
	int		x;
	int		y;
	t_obj	*tmp;
	float	dist;

	y = 0;
	while (y < all->height)
	{
		x = 0;
		while (x < all->width)
		{
			all->tab[y][x] = all->background;
			x++;
		}
		all->tab[y][x] = 0;
		y++;
	}
	while (all->elem)
	{
		y = 0;
		while (y < all->height)
		{
			x = 0;
			while (x < all->width)
			{
				dist = sqrtf(powf(x - all->elem->x, 2) + powf(y - all->elem->y, 2));
				if (dist <= all->elem->rad && (
							all->elem->type == 'C' ||
							ft_abs(dist - all->elem->rad) < 1))
					all->tab[y][x] = all->elem->fil;
				x++;
			}
			y++;
		}
		tmp = all->elem;
		all->elem = tmp->next;
		free(tmp);
	}
}

int main(int ac, char **av)
{
	t_bas	all;

	if (ac != 2)
	{
		ft_putstr("Error: argument\n");
		return (1);
	}
	if (parse(av[1], &all))
	{
		ft_putstr("Error: Operation file corrupted\n");
		return (1);
	}
	fillit(&all);
	int i = 0;
	while (i < all.height)
	{
		ft_putstr(all.tab[i++]);
		write(1, "\n", 1);
	}
	return (0);
}
