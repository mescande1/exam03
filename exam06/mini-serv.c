/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mini-serv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mescande <mescande@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/10 11:52:55 by mescande          #+#    #+#             */
/*   Updated: 2022/10/10 12:51:23 by mescande         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>

void fatal() {
  char *str = "Fatal error\n";
  write(2, str, strlen(str));
  exit(1);
}

char str_buf[42*4096], read_buf[42*4096], write_buf[42*4096+42];
int array_fd[65536], max_fd = 0, id = 0;
fd_set active, ready_read, ready_write;

void send_all(void) {
  int len = strlen(write_buf);
  for (int i = 0; i <= max_fd; ++i){
    if (FD_ISSET(i, &ready_write))
      send(i, write_buf, len, 0);
  }
}

int main(int ac, char **av) {
  if (ac != 2) {
    write(2, "Wrong number of arguments\n", 26);
    exit(1);
  }
  int sockfd = socket(AF_INET, SOCK_STREAM, 0); 
  if (sockfd == -1)
    fatal();

  FD_ZERO(&active);
  bzero(array_fd, sizeof(array_fd));
  max_fd = sockfd;

  struct sockaddr_in addr;
  socklen_t addr_len = sizeof(addr);
  bzero(&addr, addr_len);
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(2130706433); //127.0.0.1
  addr.sin_port = htons(atoi(av[1])); 

  // Binding newly created socket to given IP and verification 
  if ((bind(sockfd, (const struct sockaddr *)&addr, addr_len) != 0))
    fatal();
  if (listen(sockfd, 10) != 0)
    fatal();

  while (1) {
    ready_write = ready_read = active;
    if (select(max_fd + 1, &ready_read, &ready_write, 0, 0) < 0)
      continue ;
    for (int sock = 0; sock <= max_fd; ++sock) {
      if (FD_ISSET(sock, &ready_read)) {
        if (sock == sockfd) {
          int client = accept(sockfd, (struct sockaddr *)&addr, &addr_len);
          if (client < 0)
            continue ;
          if (client > max_fd)
            max_fd = client;
          array_fd[client] = id++;
          FD_SET(client, &active);
          sprintf(write_buf, "server: client %d just arrived\n", array_fd[client]);
          send_all(/*client*/);
        }
        else {
        }
      }
    }
  }
  return (0);
}
    /*len = sizeof(cli);
    connfd = accept(max_fd + 1, (struct sockaddr *)&cli, &len);
    if (connfd < 0)
      fatal();*/
