/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/02 23:12:12 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/07 16:57:07 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "micropaint.h"

int		ft_strlen(char *str)
{
	int i = 0;

	if (!str)
		return (0);
	while (str[i])
		i++;
	return (i);
}

void	putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

void	*memmalloc(int size)
{
	char	*ptr;

	ptr = malloc(size);
	if (!ptr)
		return ptr;
	while (size--)
		ptr[size] = 0;
	return ptr;
}

void	ft_memset(char *ptr, char c, int n)
{
	int i = 0;

	while (i < n)
		ptr[i++] = c;
}

int main(int ac, char **av)
{
	t_red var;
	int		i;
	if (ac != 2)
	{
		putstr("Error: argument\n");
		return (1);
	}
	t_shape *tmp;
	if (read_file(av[1], &var))
	{
		while(var.shape)
		{
			tmp = var.shape;
			var.shape = var.shape->next;
			free(tmp);
		}
		putstr("Error: Operation file corrupted\n");
		return (1);
	}
	i = 0;
	var.buff = memmalloc(sizeof(char *) * 301);
	while (i < var.h)
	{
		var.buff[i] = memmalloc(301);
		i++;
	}
	while (var.shape)
	{
		interpret(var);
		tmp = var.shape;
		var.shape = var.shape->next;
		free(tmp);
	}
	i = 0;
	while (i < var.h)
	{
		putstr(var.buff[i]);
		write(1, "\n", 1);
		free(var.buff[i]);
		i++;
	}
	free(var.buff);
}
