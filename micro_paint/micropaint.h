/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   micropaint.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/02 23:17:19 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/07 16:54:11 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MICROPAINT_H
# define MICROPAINT_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct	s_shape_list {
	char	shape;
	float	X;
	float	Y;
	float	w;
	float	h;
	char	c;
	struct s_shape_list	*next;
}	t_shape;

typedef struct	s_red {
	int		w;
	int		h;
	char	bg;

	t_shape		*shape;
	char		**buff;
}	t_red;

void	putstr(char *str);
int		ft_strlen(char *str);
void	*memmalloc(int size);

int		read_file(char *filename, t_red *var);
void	interpret(t_red var);

#endif
