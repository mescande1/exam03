/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   interpret.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/03 00:16:12 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/03 01:19:27 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "micropaint.h"

int ft_abs(int i)
{
	if (i < 0)
		return -i;
	return i;
}

void interpret(t_red var)
{
	int y = var.shape->Y - 1;

	while (y < var.shape->Y + var.shape->h + 1)
	{
		int x = var.shape->X - 1;
		while (x < var.shape->X + var.shape->w + 1)
		{
//			dprintf(9, "%f %f < %d %d\t< %f %f", var.shape->X, var.shape->Y, x, y, var.shape->X + var.shape->w, var.shape->Y + var.shape->h);
			if ((float)var.shape->Y <= (float)y && (float)y <= (float)var.shape->Y + (float)var.shape->h
				&& (float)var.shape->X <= (float)x && (float)x <= (float)var.shape->X + (float)var.shape->w)
			{
//				dprintf(9, "yep");
				if (var.shape->shape == 'R')
				{
					if (x >= 0 && x < var.w && y >= 0 && y < var.h)
						var.buff[y][x] = var.shape->c;
				}
				else if (
					(x >= 0 && x < var.w && y >= 0 && y < var.h)
					&& (ft_abs(x - var.shape->X) < 1 
						||ft_abs(x - (var.shape->X + var.shape->w)) < 1
						||ft_abs(y - var.shape->Y) < 1
						||ft_abs(y - (var.shape->Y + var.shape->h)) < 1))
					var.buff[y][x] = var.shape->c;
			}
//			dprintf(9, "\n");
			x++;
		}
		y++;
	}
}
