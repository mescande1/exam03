/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: matthieu <matthieu.escande@gmail.com>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/02 23:19:15 by matthieu          #+#    #+#             */
/*   Updated: 2021/12/03 01:56:40 by matthieu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "micropaint.h"

int	read_file(char *filename, t_red *var)
{
	FILE	*stream;
	t_shape	*last = NULL;
	t_shape	*tmp;
//	char	*buf = memmalloc(2049);

	stream = fopen(filename, "r");
	if (!stream)
		return (1);
	//fread(buf, 2047, 1, stream);
	fscanf(stream, "%d %d %c ", &(var->w), &(var->h), &(var->bg));
	if (var->w <= 0 || 300 < var->w || var->h <= 0 || 300 < var->h)
		return (1);
	tmp = memmalloc(sizeof(t_shape));
	int ret;
	char toto[2];
	while ((ret = fscanf(stream, "%c%1[ ] %f%1[ ] %f%1[ ] %f%1[ ] %f%1[ ] %1[^\n] ", &(tmp->shape), toto, &(tmp->X), toto, &(tmp->Y), toto, &(tmp->w), toto, &(tmp->h), toto, &(tmp->c))) != -1)
	{
		printf("%d %c\n", ret, tmp->shape);
		fflush(stdout);
		if (ret != 11 || tmp->c == '\n')
		{
			free(tmp);
			return (1);
		}
		if (tmp->w <= 0 || tmp->h <= 0)
			return (1);
		if (last == NULL)
			var->shape = tmp;
		else
			last->next = tmp;
		last = tmp;
		tmp = memmalloc(sizeof(t_shape));
	}
	free(tmp);
	return	0;
}
